//Ezt megérteni.

let allTasks = [{content:"this is the first task", done:true}];

function displayTasksFromArray(taskArray){
    document.querySelector("section > ul").innerHTML = "";
    taskArray.forEach(currentTask => displayTask(currentTask));
}

function createTask(tasks, todo){
    let task = 
    {
        content: todo,
        done: false
    };
    tasks.push(task);
    displayTask(task);
}

function displayTask(task){
    if(task && task.content !== ""){
    let template = `<li class= "${task.done ? "done": ""}"> <span class="fa ${task.done ? "fa-check-square-o": "fa-square-0"} fa-2x"></span>
            <span>${task.content}</span>
            <span class="fa fa-times"></span>
        </li>`;
document.querySelector("section > ul").insertAdjacentHTML("beforeend", template);
//addEventListener hasonló egy tulajdonsághoz. "Kattinthatóvá" teszi az elemet. 
//Ezt a Javascript program ráteszi az elemre, és utána megy tovább lefuttatja a program további részét, 
//addig nem fog lefutni az addEventListener, amíg rá nem kattint valaki.
document.querySelector("section > ul > li:last-child > span:first-of-type").addEventListener("click", markDone);
}
}

//Eventhandler for click on the square in any of the todo list. 
function markDone(event){
    event.target.parentElement.classList.add("done");
    event.target.classList.remove("fa-square-o");
    event.target.classList.add("fa-check-square-o");
    //Eltünteti a "kattinthatóságot". 
    event.target.removeEventListener("click", markDone);
    }

//Deletes a task
function deleteTask(event){
    event.target.parentElement.remove();
}
//function(event) a callback függvénye az addEventListenernek.
document.querySelector("form > button").addEventListener("click", function(event){
    createTask(document.querySelector("#todo").value);
    event.target.previousElementSibling.value = "";
});

document.querySelector("form > input").addEventListener("keypress", function(event){
    //Enterre: ne frissítse az oldalt (form tag tulajdonságai miatt), és csak az enter gomb lenyomására hívja be a createTask() függvényt.
    if(event.key === "Enter"){
    event.preventDefault(true);
    createTask(event.target.previousElementSibling.value);
    event.target.value="";}
});

document.querySelector("section > ul > li:last-child > span:first-of-type").addEventListener("click", markDone);
document.querySelector("section > ul > li:last-child > span:last-of-type").addEventListener("click", deleteTask);

displayTasksFromArray(allTasks);






























